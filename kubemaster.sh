#!/usr/bin/env bash

set -e

MASTER_IP=$1
SCRIPT_DIR=$(pwd)
NUM=$(( ( RANDOM % 89 )  + 10 ))

if [ "$MASTER_IP" == '' ]; then
  echo "usage: $0 <puppetmaster ip address>"
  exit 1
fi

time ./init/puppetagent.sh test-kubemaster.mhfa.net.au $MASTER_IP
