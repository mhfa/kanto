MHFA Cloud
==========

This is the single source of truth repository for bootstrapping MHFA cloud
for its web apps.

Puppet is used for configuration management, there are a few bash scripts to
install Puppet and for one time setups such as downloading the application
source code into a vhost directory.

Drupal (WIMS 2) and Moodle are the main apps at this point.
CentOS (RHEL) 7 is the server OS distribution.

overview
--------

Here an overview of the files and directories. Please explore each directories
for more details.

* control-repo: Puppet source code.
* init: Various helper scripts to install packages before Puppet is deployed,
  such as, installing Puppet.
*   puppetmaster.sh: Creates the first server of a MHFA environment. Sets fqdn
    of the current server to ``general-syd01.mhfa.net.au``, installs puppet and
    configures the current server as the puppetmaster.

    This script takes no parameter. It needs the internal IP address for
    ``profile::ebs::nfs_mount::server``, so please first add it to
    ``control-repo/data/config.yaml``.
* php5.sh: Takes IP address of the puppet master and joins agent to it. Assigns
  a fqdn with a random number between 10-99 to avoid name colusion.
* stage.sh: Takes a vhost name and downloads WIMS 2 and place it under
  ``/var/mhfa/ebs/apps`` to be used for staging environment.
* README.rst: This file.

getting started
---------------

A quick getting started guide assuming that you will be creating a two nodes on
EC2 instances running CentOS 7, for creating a puppetmaster node and a php5
node for serving WIMS.

Take one node and configure it as a puppetmaster as follows::

    sudo yum install git -y
    sudo mkdir /var/mhfa && sudo chown centos /var/mhfa
    git clone git@gitlab.com:mhfa/kanto.git /var/mhfa/kanto

Copy the internal IP address: ``ifconfig eth0``.

Paste it into ``/var/mhfa/kanto/control-repo/data/config.yaml``,
``profile::ebs::nfs_mount::server``, then run the set up script::

    cd /var/mhfa/kanto
    ./puppetmaster.sh

The node will be running puppetmaster after ``puppetmaster.sh`` script has
completed successfully.

Take the other node and configure it as the php node as follows::

    mkdir /tmp/init
    # Download kanto/init/puppetagent.sh into /tmp/init/
    # Download kanto/php5.sh into /tmp
    cd /tmp
    # Copy the internal IP address of the puppetmaster
    ./php5.sh <INTERNAL IP>

Please sign the certificate request on the puppetmaster a few moments later::

    # To see any outstanding requests
    /opt/puppetlabs/bin/puppet cert list
    # To sign a request
    /opt/puppetlabs/bin/puppet cert sign <HOSTNAME>

The node will be running php after ``php.sh`` script has completed
successfully.

To deploy WIMS, SSH into puppetmaster.

Add the values for the following keys into ``control-repo/data/config.yaml``:

* ``profile::rds::host``
* ``profile::rds::user``
* ``profile::rds::pass``
* ``mhfa::apps``
* ``mhfa::apps::approved_ip``
* ``mhfa::staging::username``
* ``mhfa::staging::password``

Then run the vhost deployment script for stage::

    cd /var/mhfa/kanto
    ./stage.sh <SITENAME>
    sudo puppet agent -t

Then add database details into
``/var/mhfa/ebs/apps/<SITENAME>/sites/default/settings.dev.php`` and sync the
database with ``drush``.

Add DNS record ``<SITENAME>.stage.mhfa.com.au`` to the public IP of the php
node.

WIMS will be running on ``<SITENAME>.stage.mhfa.com.au`` after the DNS record
has propagated.

maintainer
----------

Sunny Yiu <sunnyy@mhfa.com.au>
