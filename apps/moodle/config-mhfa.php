<?php
///////////////////////////////////////////////////////////////////////////
//                                                                       //
// Moodle configuration file for MHFA                                    //
//                                                                       //
// This file should be renamed "config.php" in the top-level directory.  //
// It contains MHFA specific settings and commonly used Moodle settings. //
// Many Moodle settings are left out. Please see config-dist.php and     //
// include them into config.php if they are needed.                      //
//                                                                       //
///////////////////////////////////////////////////////////////////////////
unset($CFG);  // Ignore this line
global $CFG;  // This is necessary here for PHPUnit execution
$CFG = new stdClass();
$CFG->dbtype    = 'mariadb';      // 'pgsql', 'mariadb', 'mysqli', 'mssql', 'sqlsrv' or 'oci'
$CFG->dblibrary = 'native';     // 'native' only at the moment
$CFG->dbhost    = '';
$CFG->dbname    = 'moodle';
$CFG->dbuser    = 'moodle';
$CFG->dbpass    = '';
$CFG->prefix    = 'mdl_';
$CFG->wwwroot   = 'http://elearning.mhfa.com.au';
$CFG->alternateloginurl = $CFG->wwwroot . '/user?destination=cas/login';
$CFG->environment_indicator_text = 'STAGING';
$CFG->mainsite  = 'https://mhfa.com.au';
$CFG->forced_plugin_settings['auth/cas'] = [
    'hostname' => 'mhfa.com.au',
    'port'     => '443',
];
$CFG->dataroot  = '/data';
$CFG->directorypermissions = 02777;
$CFG->admin = 'admin';
$CFG->reverseproxy = true;
$CFG->passwordsaltmain = '';
require_once(dirname(__FILE__) . '/lib/setup.php'); // Do not edit
