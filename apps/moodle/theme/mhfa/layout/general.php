<?php
$main_site_baseurl = $CFG->mainsite ? $CFG->mainsite : 'https://mhfa.com.au';
$hasheading = ($PAGE->heading);
$hasnavbar = (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar());
$hasfooter = (empty($PAGE->layout_options['nofooter']));
$hassidepre = (empty($PAGE->layout_options['noblocks']) && $PAGE->blocks->region_has_content('side-pre', $OUTPUT));
$hassidepost = (empty($PAGE->layout_options['noblocks']) && $PAGE->blocks->region_has_content('side-post', $OUTPUT));
$haslogininfo = (empty($PAGE->layout_options['nologininfo']));

$showsidepre = ($hassidepre && !$PAGE->blocks->region_completely_docked('side-pre', $OUTPUT));
$showsidepost = ($hassidepost && !$PAGE->blocks->region_completely_docked('side-post', $OUTPUT));

$custommenu = $OUTPUT->custom_menu();
$hascustommenu = (empty($PAGE->layout_options['nocustommenu']) && !empty($custommenu));

$courseheader = $coursecontentheader = $coursecontentfooter = $coursefooter = '';
if (empty($PAGE->layout_options['nocourseheaderfooter'])) {
   $courseheader = $OUTPUT->course_header();
   $coursecontentheader = $OUTPUT->course_content_header();
   if (empty($PAGE->layout_options['nocoursefooter'])) {
       $coursecontentfooter = $OUTPUT->course_content_footer();
       $coursefooter = $OUTPUT->course_footer();
   }
}

$bodyclasses = array();
if ($showsidepre && !$showsidepost) {
   if (!right_to_left()) {
       $bodyclasses[] = 'side-pre-only';
   }else{
       $bodyclasses[] = 'side-post-only';
   }
} else if ($showsidepost && !$showsidepre) {
   if (!right_to_left()) {
       $bodyclasses[] = 'side-post-only';
   }else{
       $bodyclasses[] = 'side-pre-only';
   }
} else if (!$showsidepost && !$showsidepre) {
   $bodyclasses[] = 'content-only';
}
if ($hascustommenu) {
   $bodyclasses[] = 'has_custom_menu';
}

echo $OUTPUT->doctype()

?>
<html <?php echo $OUTPUT->htmlattributes() ?>>
<head>
   <title><?php echo $PAGE->title ?></title>
   <link rel="shortcut icon" href="<?php echo $OUTPUT->pix_url('favicon', 'theme')?>" />
   <link type="text/css" rel="stylesheet" href="//assets.zendesk.com/external/zenbox/v2.6/zenbox.css" media="all" />
   <?php echo $OUTPUT->standard_head_html() ?>
</head>
<body id="<?php p($PAGE->bodyid) ?>" class="<?php p($PAGE->bodyclasses.' '.join(' ', $bodyclasses)) ?>">
<?php echo $OUTPUT->standard_top_of_body_html()

?>
<div id="page">



<?php if ($hasheading || $hasnavbar || !empty($courseheader)) { ?>
   <div id="page-header">



     <div id="branding" class="clearfix">
       <a id="logo" href="<?php echo $main_site_baseurl ?>">
         <img src="<?php echo $OUTPUT->pix_url('logo', 'theme') ?>" title="Home" />
       </a>

       <div id="primary-strip">
         <div class="main-width">

               <div id="block-mhfa-core-user-greeting" class="headermenu"><?php
                   if ($haslogininfo) {
                         $html = $OUTPUT->login_info();
                         $dom = new DomDocument();
                         $dom->loadHTML($html);
                         $links = array();
                         foreach ($dom->getElementsByTagName('a') as $node)
                         {
                           $links[] = array(
                             'text' => $node->nodeValue,
                             'href' => $node->getAttribute("href"),
                           );
                         }
                         // $parts = explode('<a', $OUTPUT->login_info());

                         // $name_parts = explode('>', $parts[1]);
                         // $name_parts = explode('<', $name_parts[1]);
                         // $name = $name_parts[0];

                         // $link_parts = explode('"', $parts[2]);
                         // $logout_path = $link_parts[1];

                         ?>
                         <a href="<?php echo $main_site_baseurl ?>/user" class="user-greeting-name home-icon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $links[0]['text'] ?></a>. <a href="<?php echo $links[1]['href'] ?>">Logout.</a>
                         <!-- <span class="user-greeting-name"><?php echo $links[0]['text'] ?></span>. <a href="<?php echo $links[1]['href'] ?>">Logout.</a> -->
                   <?php
                   //echo $OUTPUT->login_info();
                   }
                   // if (!empty($PAGE->layout_options['langmenu'])) {
                   //     echo $OUTPUT->lang_menu();
                   // }
                   echo $PAGE->headingmenu
               ?></div><?php } ?>

               <?php if ($CFG->environment_indicator_text): ?>
               <div style="font-size:50px;margin-top:20px;float: left;color: white;">
                   <strong>
                       <?php echo $CFG->environment_indicator_text; ?>
                   </strong>
               </div>
               <?php endif; ?>

         </div>
       </div>

       <!-- secondary-strip -->
       <div id="secondary-strip">
         <div class="main-width">
           <div id="page-title">

             <?php if ($hasheading) { ?>

               <h1 class="headermain"><?php echo $PAGE->heading ?></h1>

               <?php if (!empty($courseheader)) { ?>
                   <div id="course-header"><?php echo $courseheader; ?></div>
               <?php } ?>

               <?php if ($hascustommenu) { ?>
                   <div id="custommenu"><?php echo $custommenu; ?></div>
               <?php } ?>

           </div>
         </div>
       </div>
       <!-- END secondary-strip -->

     </div>
     <!-- END branding -->


       <?php if ($hasnavbar) { ?>
           <div class="navbar clearfix main-width">
               <!-- <div class="breadcrumb"><?php echo $OUTPUT->navbar(); ?></div> -->
               <div class="navbutton"> <?php echo $PAGE->button; ?></div>
           </div>
       <?php } ?>
   </div>
<?php } ?>
<!-- END OF HEADER -->




   <div id="page-content" class="main-width">
       <div id="region-main-box">
           <div id="region-post-box">

               <div id="region-main-wrap">
                   <div id="region-main">
                       <div class="region-content">
                           <?php echo $coursecontentheader; ?>
                           <?php echo $OUTPUT->main_content() ?>
                           <?php echo $coursecontentfooter; ?>
                       </div>
                   </div>
               </div>

               <?php if ($hassidepre OR (right_to_left() AND $hassidepost)) { ?>
               <div id="region-pre" class="block-region">
                   <div class="region-content">
                           <?php
                       if (!right_to_left()) {
                           echo $OUTPUT->blocks_for_region('side-pre');
                       } elseif ($hassidepost) {
                           echo $OUTPUT->blocks_for_region('side-post');
                   } ?>

                   </div>
               </div>
               <?php } ?>

               <?php if ($hassidepost OR (right_to_left() AND $hassidepre)) { ?>
               <div id="region-post" class="block-region">
                   <div class="region-content">
                          <?php
                      if (!right_to_left()) {
                          echo $OUTPUT->blocks_for_region('side-post');
                      } elseif ($hassidepre) {
                          echo $OUTPUT->blocks_for_region('side-pre');
                   } ?>
                   </div>
               </div>
               <?php } ?>

           </div>
       </div>
   </div>

<!-- START OF FOOTER -->
   <?php if (!empty($coursefooter)) { ?>
       <div id="course-footer"><?php echo $coursefooter; ?></div>
   <?php } ?>
   <?php if ($hasfooter) { ?>
   <div id="page-footer" class="clearfix">
       <p class="helplink"><?php echo page_doc_link(get_string('moodledocslink')) ?></p>
       <?php
       echo $OUTPUT->login_info();
       echo $OUTPUT->home_link();
       echo $OUTPUT->standard_footer_html();
       ?>
   </div>
   <?php } ?>
   <div class="clearfix"></div>
</div>
<?php echo $OUTPUT->standard_end_of_body_html() ?>

<!-- Start of Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(c){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("//assets.zendesk.com/embeddable_framework/main.js","mhfa.zendesk.com");/*]]>*/</script>
<!-- End of Zendesk Widget script -->

</body>
</html>
