#!/usr/bin/env bash

set -e

sudo cp bin/cfssl_linux-amd64     /usr/local/bin/cfssl
sudo cp bin/cfssljson_linux-amd64 /usr/local/bin/cfssljson
sudo cp bin/kubectl               /usr/local/bin/kubectl
