to get started:

1. make a copy of ``env-sample``
2. rename it to ``env``
3. add hostname and ip address into ``ETCD_INITIAL_CLUSTER`` variable in
   ``env``, you will need to uncomment it
4. run ``update.sh``