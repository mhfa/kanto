#!/usr/bin/bash

set -e

docker run --rm -v $(pwd):/mnt --env-file env puppet/kubetool:4.0.0
cp Centos.yaml ../control-repo/data/kubernetes.yaml 
cp test-kubemaster.mhfa.net.au.yaml ../control-repo/data/nodes/test-kubemaster.mhfa.net.au.yaml
