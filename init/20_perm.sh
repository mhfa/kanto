#!/usr/bin/env bash

# apply new permissions to the current directory and everything
# under it.

set -e

# set all groups to wheel.
sudo chgrp -R wheel .
# add write permission.
sudo chmod -R g+rw .
# new folders to inherit group from parent (wheel).
sudo chmod g+s .
find . -type d -exec sudo chmod g+s '{}' \;
