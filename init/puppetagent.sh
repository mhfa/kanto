#!/usr/bin/env bash

# installs puppet agent.
# takes the desired hostname and ip address of the puppetmaster.

HOSTNAME=$1
MASTER_IP=$2
REPO_URL='https://yum.puppet.com/puppet5/puppet5-release-el-7.noarch.rpm'
SERVER_PKG=puppetserver-5.3.5-1.el7
AGENT_PKG=puppet-agent-5.5.6-1.el7
R10K_PKG='r10k -v 2.6.4'
BUNDLER_PKG='bundler -v 1.16.6'
USAGE="usage: $0 <hostname> <puppetmaster ip address>"

set -e

if [ "$MASTER_IP" == '' ]; then
  echo $USAGE
  exit 1
fi

if [ "$HOSTNAME" != '' ]; then
  /usr/bin/sudo /usr/bin/hostnamectl set-hostname "$HOSTNAME"
else
  echo $USAGE
  exit 1
fi

if [ -f /opt/puppetlabs/bin/puppet ]; then
  echo 'Puppet is already installed.'
  exit 0
fi

echo 'Configuring PuppetLabs repo...'
sudo /usr/bin/yum localinstall $REPO_URL -y
sudo /usr/bin/yum clean all
sudo /usr/bin/yum makecache

echo 'Installing puppet'
sudo /usr/bin/yum install $AGENT_PKG -y -q

echo 'Puppet installed!'

# add puppet to hosts file.
sudo /opt/puppetlabs/puppet/bin/puppet resource host puppet ensure=present ip="$MASTER_IP"
sudo /opt/puppetlabs/puppet/bin/puppet resource host $HOSTNAME ensure=present ip='127.0.0.1'

# join agent to master.
echo 'please head over to the master and sign the request for:'
echo $HOSTNAME
sudo /opt/puppetlabs/puppet/bin/puppet agent --test --waitforcert 10

# start puppet.
#sudo /opt/puppetlabs/puppet/bin/puppet resource service puppet ensure=running enable=true

# install bundler.
sudo /opt/puppetlabs/puppet/bin/gem install $BUNDLER_PKG
sudo /usr/bin/ln -s /opt/puppetlabs/puppet/bin/bundle /opt/puppetlabs/bin/
