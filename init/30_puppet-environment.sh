#!/usr/bin/env bash

REPO=$1
PROD=/etc/puppetlabs/code/environments/production

set -e

if [ "$REPO" = '' ]; then
  echo "usage: $0 <control-repo>"
  exit 1
fi

sudo rm -rf $PROD
sudo ln -s $REPO $PROD
ls -l /etc/puppetlabs/code/environments
