#!/usr/bin/env bash

SWAPPINESS=2
SIZE=10000
FILE=10GB.swap
PART=/var/mhfa/ebs
MISC=/var/mhfa/ebs/misc

set -e

if [ ! -d "$PART" ]; then
  echo no ebs partition
  exit 1
fi

sudo mkdir -p "$MISC"
sudo dd if=/dev/zero of="$MISC/$FILE" bs=1MiB count=$SIZE
sudo chmod 0600 "$MISC/$FILE"
sudo mkswap "$MISC/$FILE"
sudo swapon "$MISC/$FILE"
sudo sysctl -w vm.swappiness=$SWAPPINESS
grep -q "$FILE" /etc/fstab || echo "$MISC/$FILE none swap sw 0 0" | sudo tee -a /etc/fstab > /dev/null
