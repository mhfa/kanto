#!/usr/bin/env bash

# installs puppet 5 server, agent, and r10k.
# runs the puppetserver and joins agent to itself.
# takes 1 argument and sets the hostname and hosts file.

HOSTNAME=$1
REPO_URL='https://yum.puppet.com/puppet5/puppet5-release-el-7.noarch.rpm'
SERVER_PKG=puppetserver-5.3.5-1.el7
AGENT_PKG=puppet-agent-5.5.6-1.el7
R10K_PKG='r10k -v 2.6.4'
BUNDLER_PKG='bundler -v 1.16.6'

set -e

if [ "$EUID" -ne '0' ]; then
  echo 'This script must be run as root.' >&2
fi

if [ "$HOSTNAME" != '' ]; then
  sudo hostnamectl set-hostname "$HOSTNAME"
else
  echo "usage: $0 <hostname>"
  exit 1
fi

if [ -f /opt/puppetlabs/bin/puppet ]; then
  echo 'Puppet is already installed.'
  exit 0
fi

echo 'Configuring PuppetLabs repo...'
sudo yum localinstall $REPO_URL -y
sudo yum clean all
sudo yum makecache

echo 'Installing puppet'
sudo yum install $SERVER_PKG $AGENT_PKG -y -q

echo 'Puppet installed!'

# add puppet to hosts file.
sudo /opt/puppetlabs/puppet/bin/puppet resource host puppet ensure=present ip='127.0.0.1'
sudo /opt/puppetlabs/puppet/bin/puppet resource host $HOSTNAME ensure=present ip='127.0.0.1'

# set java heap size for puppet master.
sudo /opt/puppetlabs/puppet/bin/puppet resource augeas puppetserver context=/files/etc/sysconfig/puppetserver changes="set JAVA_ARGS '\"-Xms512m -Xmx512m\"'"

# start puppetserver and puppet.
sudo /opt/puppetlabs/puppet/bin/puppet resource service puppetserver ensure=running enable=true
#sudo /opt/puppetlabs/puppet/bin/puppet resource service puppet ensure=running enable=true

# join agent to itself.
sudo /opt/puppetlabs/puppet/bin/puppet agent -t

# install r10k.
sudo /opt/puppetlabs/puppet/bin/gem install $R10K_PKG
sudo ln -s /opt/puppetlabs/puppet/bin/r10k /opt/puppetlabs/bin/

# install bundler.
sudo /opt/puppetlabs/puppet/bin/gem install $BUNDLER_PKG
sudo ln -s /opt/puppetlabs/puppet/bin/bundle /opt/puppetlabs/bin/
