Data directory for hiera lookup. Both ``common.yaml`` and ``config.yaml`` are
available in all nodes. Data in ``config.yaml`` overrides ``common.yaml``.

The ``config.yaml`` file is intentionally ingored in git so it can be used for
environment specific values. Note, you SHOULD NOT store secrets in plain text
files, please employ proper tools for securing secret data.
