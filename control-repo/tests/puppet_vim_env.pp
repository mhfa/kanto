puppet_vim_env::install { 'mhfa':
  owner       => 'mhfa',
  homedir     => '/home/mhfa',
  colorscheme => 'default',
}
