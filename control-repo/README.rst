mhfa puppet control-repo
========================

the mhfa puppet codebase, based off puppetlab's control-repo:
https://github.com/puppetlabs/control-repo

the important files and items in this codebase:

*   environment.conf that correctly implements:

    *   site directory for roles, profiles, and any custom modules. (see
        https://puppet.com/docs/puppet/5.3/config_file_environment.html)

    *   config_version script.

* puppetfile to manage external modules.

* hiera configuration file and data directory.

* config_version script that outputs the git commit id of the code that was
  used during a puppet run. (see
  https://puppet.com/docs/puppet/5.3/config_file_environment.html#configversion)

to test site.pp with puppet apply::

    sudo /opt/puppetlabs/bin/puppet apply --modulepath='site:modules:$basemodulepath' manifests/site.pp

to test other puppet files::

    sudo /opt/puppetlabs/bin/puppet apply --modulepath='site:modules:$basemodulepath' tests/puppetdb.pp

puppetfile
----------

specifies a list of modules to install, what version to install, and where to
fetch them from.

install or update all modules into ./modules::

    r10k puppetfile install

remove any modules in the 'modules' directory that are not specified::

    r10k puppetfile purge

verify the puppetfile syntax::

    r10k puppetfile check

note: the r10k implementation of puppetfile does not include dependency
resolution, but it is on the roadmap.

node definition and hiera
-------------------------

``manifests/site.pp.example`` and ``data/common.yaml`` are provided as
example default node definition and hiera data to give you an idea of
the type of values that can go in there.

please copy those as ``manifests/site.pp`` and ``data/config.yaml`` and
populate environment specific data, for example, enter the hostname of
your nfs server of your staging environment into ``config.yaml``. these
are intentionally added to gitignore and may contain sensitive
production data if required.

to start::

    cp manifests/site.pp.example manifests/site.pp
    cp data/common.yaml config.yaml

note, you SHOULD NOT store secrets in plain text files, please employ
proper tools for securing secret data.
