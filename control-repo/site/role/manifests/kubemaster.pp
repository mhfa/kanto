# kubernetes master.
class role::kubemaster {
  include profile::base
  include profile::kubemaster
}
