# this role provides php website(s).
class role::php5 {
  include profile::base
  include profile::memcached
  include profile::php::php5_apache
  include profile::php::drush
  include profile::mysqlclient
  include profile::docker
}
