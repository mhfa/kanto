# class role::general
# runs the 'backend' and other general stuff, like nfs, puppet master, etc.
# acts as a central point of access for dev to deploy updates.
class role::general {
  include profile::base
  include profile::puppetserver
  include profile::puppetdb
  include profile::docker
  include profile::rds
  include profile::php::php5
  include profile::php::drush
  include profile::awscli
}
