# the base profile includes component modules that will be on all nodes.
class profile::base {
  include profile::common
  include profile::firewall
  include profile::vim
  include profile::ssh_config
  include profile::sshd
  include profile::ebs
  include profile::mail
  include profile::wkhtmltopdf
}
