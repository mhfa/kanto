class profile::puppetserver {
  package { 'puppetserver':
    ensure => installed,
  } ->
  service { 'puppetserver':
    ensure => running,
    enable => true,
  }
}
