# ssh server settings.
# port 1 is usually used. port 22 opens briefly (for that day) after
# each reboot in case there's a need for an engineer to troubleshoot.
# supports mosh: https://github.com/mobile-shell/mosh
class profile::sshd {
  include firewall

  # ssh server listens to port 1 and 22.
  class { 'ssh::server':
    ensure               => latest,
    storeconfigs_enabled => false,
    validate_sshd_file   => true,
    options              => {
      'Port'                   => [1, 22],
      'AcceptEnv'              => [
        'GIT_COMMITTER_NAME',
        'GIT_COMMITTER_EMAIL',
        'GIT_AUTHOR_NAME',
        'GIT_AUTHOR_EMAIL',
      ],
      'PasswordAuthentication' => 'no',
      'PermitRootLogin'        => 'no',
    }
  }

  package { ['mosh', 'at']:
    ensure  => latest,
    require => Package['epel-release'],
  }

  # allows ssh and mosh ports through the firewall.
  firewall { '010 ssh':
    dport  => [1, 22],
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '010 mosh':
    dport  => '60000-61000',
    proto  => 'udp',
    action => 'accept',
  }

  # configure selinux to allow ssh traffic on port 1.
  selinux::port { 'ssh-port1':
    ensure   => present,
    seltype  => 'ssh_port_t',
    protocol => 'tcp',
    port     => 1,
    notify   => Service['sshd'],
  }

  # block port 22 entirely late night after each reboot.
  cron { 'close_port_22':
    command => 'echo \'/usr/sbin/iptables -I INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j REJECT --reject-with icmp-host-prohibited\' | at 1am', # lint:ignore:140chars
    user    => 'root',
    special => 'reboot',
  }
}
