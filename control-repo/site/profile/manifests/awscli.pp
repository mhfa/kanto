# install and configure awscli.
class profile::awscli (
  $access_key = lookup('profile::awscli::access_key', String),
  $secret_key = lookup('profile::awscli::secret_key', String),
) {
  package { 'python2-pip':
    ensure => installed,
  }

  package { 'awscli':
    ensure   => '1.16.111',
    provider => pip,
    require  => Package['python2-pip'],
  }

  file { '/home/mhfa/.aws':
    ensure => directory,
    owner  => 'mhfa',
    group  => 'mhfa',
    mode   => '0700',
  }

  file { '/home/mhfa/.aws/config':
    content => "[default]\noutput = json\nregion = ap-southeast-2\n",
    owner   => 'mhfa',
    group   => 'mhfa',
    mode    => '0600',
    require => User['mhfa'],
  }

  file { '/home/mhfa/.aws/credentials':
    content => epp('profile/aws_credentials.epp', {
      access_key => $access_key,
      secret_key => $secret_key,
    }),
    owner   => 'mhfa',
    group   => 'mhfa',
    mode    => '0600',
    require => User['mhfa'],
  }
}
