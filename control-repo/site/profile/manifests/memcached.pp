# run memcached with 50% ram.
class profile::memcached {
  class { '::memcached':
    listen_ip  => '0.0.0.0',
    max_memory => '50%',
  }
}
