# baseline firewall rule: allow local traffic.
# this assumes running on aws ec2, where first nic is local.
class profile::firewall {
  include firewall

  $network = $facts['networking']['network']
  $netmask = $facts['networking']['netmask']

  firewall { '010 local network':
    proto  => ['tcp', 'udp'],
    source => "${network}/${netmask}",
    action => 'accept',
  }
}
