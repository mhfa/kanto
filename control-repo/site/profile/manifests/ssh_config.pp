# ssh client system-wide configuration
class profile::ssh_config {
  $git_vars = [
    'GIT_COMMITTER_NAME',
    'GIT_COMMITTER_EMAIL',
    'GIT_AUTHOR_NAME',
    'GIT_AUTHOR_EMAIL',
  ]
  $sendenv = join($git_vars, ' ')

  $mhfa_hosts = [
    'backend-syd01.mhfa.net.au',
    'drupal-syd01.mhfa.net.au',
    'moodle-syd01.mhfa.net.au',
    'stage-syd01.mhfa.net.au',
    'cache-syd01.mhfa.net.au',
    'stage-syd02.mhfa.net.au',
    'stage-syd03.mhfa.net.au',
    'sqldb-syd01.mhfa.net.au',
    '*.mhfa.net.au',
  ]
  $hosts = join($mhfa_hosts, ' ')

  class { '::ssh::client':
    storeconfigs_enabled => false,
    options              => {
      'VisualHostKey'                => 'yes',
      'SendEnv'                      => $sendenv,
      'ServerAliveInterval'          => 300,
      'ServerAliveCountMax'          => 3,
      'Host *.compute.amazonaws.com' => {
        'User'                  => 'mhfa',
        'StrictHostKeyChecking' => 'no',
        'UserKnownHostsFile'    => '/dev/null',
        'Port'                  => 1,
      },
      "Host ${hosts}"                => {
        'User' => 'mhfa',
        'Port' => 1,
      },
      'Host *.mylabserver.com'       => {
        'User'                  => 'cloud_user',
        'StrictHostKeyChecking' => 'no',
        'UserKnownHostsFile'    => '/dev/null',
      }
    },
  }
}
