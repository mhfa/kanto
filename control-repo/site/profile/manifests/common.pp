# common resources for base profile.
class profile::common {
  package { 'puppet':
    ensure => installed,
  }
  ->service { 'puppet':
    ensure => running,
    enable => true,
  }

  $installed = [
    'epel-release',
    'bash-completion',
    'bash-completion-extras',
  ]

  package { $installed:
    ensure => installed,
  }

  $latest = [
    'git',
    'tree',
    'mlocate',
    'curl',
    'python36-pip',
    'tmux',
  ]

  package { $latest:
    ensure => latest,
  }

  file { '/usr/local/bin/pip3':
    ensure  => link,
    target  => '/usr/bin/pip3.6',
    require => Package['python36-pip'],
  }

  package { 'random_cli':
    ensure   => latest,
    provider => pip3,
    require  => Package['python36-pip'],
  }

  yum::group { 'Development Tools':
    ensure  => installed,
  }

  class { 'selinux':
    mode => 'enforcing',
  }

  file { '/etc/localtime':
    ensure => link,
    target => '/usr/share/zoneinfo/Australia/Melbourne',
  }

  file { '/etc/gitconfig':
    source => 'puppet:///modules/profile/base/gitconfig',
    owner  => 'root',
    group  => 'root',
    mode   => '0444',
  }

  file { '/etc/profile.d/profile.sh':
    source => 'puppet:///modules/profile/base/profile.sh',
    owner  => 'root',
    group  => 'root',
    mode   => '0444',
  }

  file { '/etc/profile.d/git-completion.bash':
    source => 'puppet:///modules/profile/base/git-completion.bash',
    owner  => 'root',
    group  => 'root',
    mode   => '0444',
  }

  # mhfa general administrative acccount.
  user { 'mhfa':
    ensure         => present,
    uid            => 1100,
    password       => '*',
    gid            => 'mhfa',
    groups         => ['wheel'],
    home           => '/home/mhfa',
    managehome     => true,
    purge_ssh_keys => true,
    require        => Group['mhfa'],
  }

  group { 'mhfa':
    ensure => present,
    gid    => 1100,
  }

  ssh_authorized_key { 'sun@stormbreaker':
    user    => 'mhfa',
    type    => 'ssh-rsa',
    key     => chomp(file('profile/ssh/sunny-mhfa-notebook.pub.txt')),
    require => User['mhfa'],
  }

  ssh_authorized_key { 'maria@tiger':
    user    => 'mhfa',
    type    => 'ssh-rsa',
    key     => chomp(file('profile/ssh/mariat.pub.txt')),
    require => User['mhfa'],
  }

  ssh_authorized_key { 'graemew@Moon-Rabbit':
    user    => 'mhfa',
    type    => 'ssh-rsa',
    key     => chomp(file('profile/ssh/graemew.pub.txt')),
    require => User['mhfa'],
  }

  ssh_authorized_key { 'aditir@pizza':
    user    => 'mhfa',
    type    => 'ssh-rsa',
    key     => chomp(file('profile/ssh/aditi.pub.txt')),
    require => User['mhfa'],
  }

  ssh_authorized_key { 'rene':
    user    => 'mhfa',
    type    => 'ssh-rsa',
    key     => chomp(file('profile/ssh/rene.pub.txt')),
    require => User['mhfa'],
  }

  ssh_authorized_key { 'sun@laptop2':
    user    => 'mhfa',
    type    => 'ssh-rsa',
    key     => chomp(file('profile/ssh/sunny-notebook2.pub.txt')),
    require => User['mhfa'],
  }

  ssh_authorized_key { 'sunny@juicessh':
    user    => 'mhfa',
    type    => 'ssh-rsa',
    key     => chomp(file('profile/ssh/sunny-juicessh.pub.txt')),
    require => User['mhfa'],
  }

  file { '/etc/sudoers.d/mhfa-sudoers':
    source => 'puppet:///modules/profile/base/mhfa-sudoers',
    owner  => 'root',
    group  => 'root',
    mode   => '0440',
  }
}
