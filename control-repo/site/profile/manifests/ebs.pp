# if /dev/xvdb exists, prepares and mounts it.
# we create ebs on xvdb as a convention.
class profile::ebs {
  if 'xvdb' in $facts['disks'] {
    class { 'profile::ebs::partition': }->class { 'profile::ebs::mount': }
    include profile::ebs::nfs_export
  }
  else {
    include profile::ebs::nfs_mount
  }
}
