# installs docker and docker-compose.
class profile::docker {
  class { '::docker':
    docker_users => [ 'mhfa' ],
    version      => '18.09.2',
  }
  ->class { '::docker::compose':
    ensure  => present,
    version => '1.23.2',
  }
}
