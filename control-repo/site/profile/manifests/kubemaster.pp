# kubernetes master profile.
class profile::kubemaster {
  class { '::kubernetes':
    controller          => true,
    etcd_version        => '3.3.12',
    etcd_hostname       => $facts['fqdn'],
    node_label          => $facts['fqdn'],
    docker_yum_baseurl  => 'https://download.docker.com/linux/centos/7/x86_64/stable',
    docker_yum_gpgkey   => 'https://download.docker.com/linux/centos/gpg',
    docker_package_name => 'docker-ce',
    docker_version      => '18.09.2',
  }

  # add kubectl access to mhfa user.
  file { '/home/mhfa/.kube':
    ensure  => directory,
    owner   => 'mhfa',
    require => Class['kubernetes'],
  }
  ->file { '/home/mhfa/.kube/config':
    ensure => file,
    owner  => 'mhfa',
    source => 'file:/etc/kubernetes/admin.conf',
  }
}
