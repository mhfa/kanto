# creates db in rds if they are not there.
class profile::rds (
  $host = lookup('profile::rds::host', String),
  $user = lookup('profile::rds::user', String),
  $pass = lookup('profile::rds::pass', String),
) {
  # set up rds credentials for ::mysql classes.
  file { '/root/.my.cnf':
    ensure  => file,
    content => epp('profile/rds.mycnf.epp' , {
      host => $host,
      user => $user,
      pass => $pass,
    }),
    owner   => 'root',
    group   => 'root',
    mode    => '0400',
  }

  $apps = lookup('mhfa::apps', Hash)
  $apps.each |String $app_name, Hash $app| {
    ::mysql::db { $app_name:
      user     => $app_name,
      password => $app['dbpass'],
      host     => '%',
      grant    => 'ALL',
      charset  => 'utf8',
      collate  => 'utf8_general_ci',
      require  => File['/root/.my.cnf'],
    }
  }
}
