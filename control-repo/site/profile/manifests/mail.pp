# configures reroute emails to testing if not sent from localhost.
#
# this is done so local emails do not end up in our testing inbox.
# for example this includes system cron, clean ups, that are meant to
# be sent to root@localhost.
class profile::mail {
  package { [ 'postfix', 'mailx' ]:
    ensure => latest,
    notify => Service['postfix'],
  }
  ->augeas { 'main_cf':
    context => '/files/etc/postfix/main.cf',
    changes => 'set virtual_alias_maps pcre:/etc/postfix/virtual',
    notify  => Service['postfix'],
  }

  # if from address ends with @fqdn, this is a local email.
  # our app should send from an address ending with @mhfa.com.au.
  # reroute to testing@ if this is not a local email.
  file { '/etc/postfix/virtual':
    content => "/.*@(?!${facts['fqdn']}$).*/ testing@mhfa.com.au\n",
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    notify  => Service['postfix'],
  }

  service { 'postfix':
    ensure => running,
    enable => true,
  }
}
