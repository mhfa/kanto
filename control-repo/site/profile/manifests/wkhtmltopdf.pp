# Class: profile::wkhtmltopdf
#
# Installs wkhtmltopdf 12.3 and required packages and scripts to be used by web
# apps.
#
# The current version is distributed as a generic linux binary, stored in the
# Puppet files folder.
#
# @todo create a package from source and install that via a package manager.
class profile::wkhtmltopdf {
  # Install required packages.
  $wkhtmltopdf_req_packages = [
    'zlib',
    'urw-fonts',
    'fontconfig',
    'freetype',
    'libX11',
    'libXext',
    'libXrender',
  ]

  package { $wkhtmltopdf_req_packages:
    ensure => installed,
  }
  ->file { '/usr/local/bin/wkhtmltopdf':
    source => 'puppet:///modules/profile/base/wkhtmltopdf',
    mode   => '0555',
  }

  # Install fonts.
  file { '/var/mhfa/ebs/tmp':
    ensure => directory,
  }
  ->file { '/var/mhfa/ebs/tmp/webcore-fonts-3.0-1.noarch.rpm':
    source => 'puppet:///modules/profile/rpm/webcore-fonts-3.0-1.noarch.rpm',
    owner  => 'root',
    group  => 'root',
    mode   => '0444',
  }
  ->package { 'webcore-fonts':
    provider => 'rpm',
    source   => '/var/mhfa/ebs/tmp/webcore-fonts-3.0-1.noarch.rpm',
  }
}
