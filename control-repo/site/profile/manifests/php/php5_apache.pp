# provides php 5 server with apache.
class profile::php::php5_apache {
  include selinux

  class { 'profile::php::php5':
    fpm => true,
  }

  # apache
  class { 'apache':
    package_ensure   => latest,
    mpm_module       => 'event',
    servername       => false,
    server_signature => 'Off',
    server_tokens    => 'Prod',
    timeout          => 300,
  }

  # apache modules to execute php apps.
  class { 'apache::mod::proxy': } -> apache::mod { 'proxy_fcgi': }

  # block httpoxy: https://httpoxy.org/.
  apache::mod { 'headers': }
  ->file { '/etc/httpd/conf.d/httpoxy.conf':
    ensure  => file,
    content => "RequestHeader unset Proxy early\n",
    owner   => 'root',
    group   => 'root',
    mode    => '0664',
    notify  => Service['httpd'],
  }

  # apache selinux booleans.
  selinux::boolean { 'httpd_use_nfs':
    ensure     => 'on',
    persistent => true,
  }
  selinux::boolean { 'httpd_can_network_memcache':
    ensure     => 'on',
    persistent => true,
  }
  selinux::boolean { 'httpd_can_network_connect_db':
    ensure     => 'on',
    persistent => true,
  }
  selinux::boolean { 'httpd_can_network_connect':
    ensure     => 'on',
    persistent => true,
  }
  selinux::boolean {  'httpd_can_sendmail':
    ensure     => 'on',
    persistent => true,
  }

  # basic sample html and php pages to show if this worked.
  file { '/var/mhfa/ebs/test':
    ensure => directory,
    owner  => 'mhfa',
    group  => 'apache',
    mode   => '2775',
  }->file { '/var/mhfa/ebs/test/index.html':
    ensure  => file,
    content => "testing... [ok]\n",
    owner   => 'mhfa',
    group   => 'apache',
    mode    => '0664',
  }->file { '/var/mhfa/ebs/test/test.php':
    ensure  => file,
    content => '<?php phpinfo();',
    owner   => 'mhfa',
    group   => 'apache',
    mode    => '0664',
  }

  # create staging htpasswd.
  $username = lookup('mhfa::staging::username')
  $password = lookup('mhfa::staging::password')
  if $username != undef and $password != undef {
    $passhash = apache_pw_hash($password)
    $htpasswd = "${username}:${passhash}"
    file { '/etc/httpd/staging_htpasswd':
      ensure  => file,
      content => $htpasswd,
      owner   => 'apache',
      group   => 'apache',
      mode    => '0444',
    }
    $staging_htpasswd = '/etc/httpd/staging_htpasswd'
  }

  # all paths of wims 2 drupal directories with .htaccess file.
  $htaccess_paths = [
    'profiles/mhfa/config',
    'profiles/mhfa/libraries/tcpdf/tools',
    'sites/default/files',
    'sites/default/files/config',
    'sites/default/private',
    'sites/default/private/wordpress',
  ]
  # create a vhost for each mhfa app.
  $apps = lookup('mhfa::apps', Hash)
  $apps.each |String $app_name, Hash $app| {
    $app_path = $app['path']
    if ('drupal7' == $app['type']) {
      ::apache::vhost { $app['vhost']:
        vhost_name       => '*',
        port             => '80',
        docroot          => $app_path,
        options          => [ 'None', 'FollowSymLinks' ],
        override         => [ 'None' ],
        proxy_pass_match => [
          {
            'path'   => '^/.*\.phps?$',
            'url'    => "fcgi://localhost:9000${app_path}/",
            'params' => { 'timeout' => 300 },
          },
        ],
        custom_fragment  => epp('profile/apache_vhost_custom_fragment.conf.epp' , {
          app_path       => $app_path,
          htaccess_paths => $htaccess_paths,
          approved_ip    => lookup('mhfa::apps::approved_ip'),
          htpasswd       => $staging_htpasswd,
        }),
      }
    }
    elsif ('proxy' == $app['type']) {
      ::apache::vhost { $app['vhost']:
        vhost_name     => '*',
        port           => '80',
        docroot        => false,
        manage_docroot => false,
        proxy_pass     => [
          {
            'path' => '/',
            'url'  => $app_path,
          },
        ],
      }
    }
  }

  # explicitly enforce order of our resources.
  Selinux::Boolean <| |>
  -> Apache::Mod <| |>
  -> Apache::Vhost <| |>
  -> Service <| name == 'httpd' |>

  # standard ports for web server.
  firewall { '010 web':
    dport  => [80, 443],
    proto  => 'tcp',
    action => 'accept',
  }
}
