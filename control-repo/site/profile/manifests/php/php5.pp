# basic php profile to configure php-fpm service and php-cli.
class profile::php::php5 (
  Boolean $fpm = false,
) {
  class { '::php':
    ensure       => latest,
    manage_repos => false,
    fpm          => $fpm,
    dev          => true,
    composer     => true,
    pear         => true,
    phpunit      => false,
    settings     => {
      'PHP/max_execution_time'  => '300',
      'PHP/max_input_time'      => '300',
      'PHP/memory_limit'        => '300M',
      'PHP/post_max_size'       => '700M',
      'PHP/upload_max_filesize' => '700M',
      'PHP/expose_php'          => 'off',
      'PHP/display_errors'      => 'off',
      'PHP/session.save_path'   => '/var/tmp',
      'Date/date.timezone'      => 'Australia/Melbourne',
    },
    cli_settings => {
      'PHP/memory_limit'   => '1000M',
      'PHP/display_errors' => 'on',
    },
    extensions   => {
      'gd'             => { },
      'intl'           => { },
      'ldap'           => { },
      'mbstring'       => { },
      'mysqlnd'        => { },
      'pdo'            => { },
      'uploadprogress' => {
        provider => 'pecl',
      },
      'xdebug'         => {
        source   => 'php-pecl-xdebug',
        zend     => true,
        settings => {
          'zend_extension'           => '/usr/lib64/php/modules/xdebug.so',
          'xdebug.max_nesting_level' => 800,
        },
      },
    },
  }

  # install memcache and set hash_strategy to consistent allows memcached
  # servers to be added or removed from the pool without causing keys to be
  # remapped, and allows drupal memcache module to be used with multiple
  # memcached servers. see "multiple servers" in:
  #
  # https://cgit.drupalcode.org/memcache/tree/README.txt?id=refs/heads;id2=7.x-1.x
  #
  # and hash strategy:
  #
  # https://secure.php.net/manual/en/memcache.ini.php#ini.memcache.hash-strategy
  ::php::extension { 'memcache':
    ensure   => installed,
    source   => 'php-pecl-memcache',
    settings => { 'memcache.hash_strategy' => 'consistent' },
  }

  # installing these with package resource instead of php class,
  # as the php class generates conf files that causes problems.
  package { [ 'php-pecl-zendopcache', 'php-xml' ]:
    ensure => latest,
  }
}
