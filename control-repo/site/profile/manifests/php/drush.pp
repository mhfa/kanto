# installs drush.
#
# currently just copies drush php archive into local bin directory
# until there's a good way to install drush via a package manager.
class profile::php::drush {
  file { '/usr/local/bin/drush':
    source => 'puppet:///modules/profile/base/drush.phar',
    owner  => 'root',
    group  => 'root',
    mode   => '0555',
  }
}
