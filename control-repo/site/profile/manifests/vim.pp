# vim installation and configuration.
class profile::vim {
  package { 'vim':
    ensure => latest,
  }

  $vim_dirs = [
    '/var/tmp/vim',
    '/var/tmp/vim/backup',
    '/var/tmp/vim/swap',
    '/var/tmp/vim/undo',
  ]

  # configures vimrc for specific users with jpadams-puppet_vim_env module.
  # that module defines a declared resource "puppet_vim_env::install".
  # we can add more settings via raw_vimrc_extras.
  puppet_vim_env::install { 'mhfa':
    owner            => 'mhfa',
    homedir          => '/home/mhfa',
    colorscheme      => 'default',
    raw_vimrc_extras => chomp(file('profile/vim/vimrc')),
    require          => [
      User['mhfa'],
      File[$vim_dirs],
    ],
  }

  file { $vim_dirs:
    ensure => directory,
    mode   => '1777',
  }

  file { '/home/mhfa/.vim/bundle/vim-fugitive':
    ensure  => directory,
    recurse => true,
    source  => 'puppet:///modules/profile/vim/vim-fugitive',
    mode    => '0700',
    owner   => 'mhfa',
    group   => 'mhfa',
    require => File['/home/mhfa/.vim/bundle'],
  }
}
