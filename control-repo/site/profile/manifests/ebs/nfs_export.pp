# make ebs available via nfs.
class profile::ebs::nfs_export (
  $path = lookup('profile::ebs::path', {value_type => String, default_value => '/var/mhfa/ebs'})
) {
  class { '::nfs':
    server_enabled => true,
  }

  nfs::server::export { $path:
    ensure  => mounted,
    clients => 'localhost(rw) *(rw,insecure,async,no_root_squash)',
  }
}
