# mounts ebs partition.
class profile::ebs::mount {
  file { '/var/mhfa/ebs':
    ensure  => directory,
    owner   => 'mhfa',
    group   => 'wheel',
    mode    => '2775',
    require => User['mhfa'],
  }->mount { '/var/mhfa/ebs':
    ensure => mounted,
    device => '/dev/xvdb1',
    fstype => 'ext4',
    atboot => true,
  }
}
