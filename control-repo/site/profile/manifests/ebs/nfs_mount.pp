# mount ebs via nfs from the server.
class profile::ebs::nfs_mount (
  $server = lookup('profile::ebs::nfs_mount::server', {default_value => undef}),
  $path = lookup('profile::ebs::path', {value_type => String, default_value => '/var/mhfa/ebs'}),
) {
  if $server {
    class { '::nfs':
      client_enabled => true,
    }

    nfs::client::mount { $path:
      server => $server,
    }
  }
  else {
    notify { 'not mounting ebs. profile::ebs::nfs_mount::server undefined.': }
  }
}
