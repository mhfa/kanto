# create ebs partition (xvdb1) if it is not there.
class profile::ebs::partition {
  if !('/dev/xvdb1' in $facts['partitions']) {
    package { [ 'parted', 'e2fsprogs' ]:
      ensure => installed,
    }->exec { 'create xvdb1':
      command => '/usr/sbin/parted -s -a optimal /dev/xvdb -- mklabel gpt mkpart primary 0% 100%',
    }->exec { 'refresh xvdb1':
      command => '/usr/sbin/partprobe && /usr/bin/sleep 1',
    }->exec { 'format xvdb1':
      command => '/usr/sbin/mkfs.ext4 /dev/xvdb1',
    }
  }
}
