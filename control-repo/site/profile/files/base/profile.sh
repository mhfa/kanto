# file managed by puppet.
export CLICOLOR=1
export LSCOLORS=Gxfxcxdxbxegedabagacad
export VISUAL=vim
export EDITOR="$VISUAL"
alias see='vim -R'
alias grep='grep --colour=auto'
alias tssh='ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
alias tscp='scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
alias tsftp='sftp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
alias trsync='rsync -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"'
