#!/usr/bin/env python

'''
returns first 8 character of the last commit for a given a path.
'''

import sys
import subprocess

cwd = sys.argv[1]
cmd = ['/usr/bin/git', 'rev-parse', 'HEAD']
commit_hash = subprocess.check_output(cmd, cwd=cwd).strip()
print(commit_hash[0:8])
