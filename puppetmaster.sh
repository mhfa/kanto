#!/usr/bin/env bash

set -e

FQDN=general-syd01.mhfa.net.au
KANTO=$(pwd)
PROD=$KANTO/control-repo

./init/10_puppetmaster.sh $FQDN
./init/20_perm.sh
./init/30_puppet-environment.sh $PROD
cd $PROD
sudo /opt/puppetlabs/bin/puppet apply --modulepath='site:modules:$basemodulepath' manifests/site.pp
sudo /opt/puppetlabs/bin/puppet agent -t
sudo /opt/puppetlabs/bin/puppet agent -t
