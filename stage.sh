#!/usr/bin/env bash

set -e

NAME="$1"
SCRIPT_DIR=$(pwd)
HTTPD_GID=48

if [ "$NAME" == '' ]; then
  echo "usage: $0 <appname>"
  exit 1
fi

git clone git@bitbucket.org:mhfaint/mhfa_platform_wide.git "/var/mhfa/ebs/apps/$NAME"
ln -s /var/mhfa/ebs/drupal_files "/var/mhfa/ebs/apps/$NAME/sites/default/files"
mkdir "/var/mhfa/ebs/apps/$NAME/sites/default/private"
mkdir "/var/mhfa/ebs/apps/$NAME/sites/default/private/wordpress"
cp /var/mhfa/ebs/apps/settings.dev.php "/var/mhfa/ebs/apps/$NAME/sites/default/"
cp "/var/mhfa/ebs/apps/$NAME/sites/default/default.settings.php" "/var/mhfa/ebs/apps/$NAME/sites/default/settings.php"
echo "require_once('settings.dev.php');" >> "/var/mhfa/ebs/apps/$NAME/sites/default/settings.php"
sudo chgrp -R $HTTPD_GID "/var/mhfa/ebs/apps/$NAME/sites/default"
sudo chmod a-w "/var/mhfa/ebs/apps/$NAME/sites/default"
